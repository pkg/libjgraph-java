Source: libjgraph-java
Section: java
Priority: extra
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders:
 tony mancill <tmancill@debian.org>,
 gregor herrmann <gregoa@debian.org>
Build-Depends:
 ant,
 debhelper (>= 10),
 default-jdk,
 maven-repo-helper
Standards-Version: 3.9.8
Homepage: https://github.com/jgraph/jgraphx
Vcs-Git: https://anonscm.debian.org/git/pkg-java/libjgraph-java.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-java/libjgraph-java.git

Package: libjgraph-java
Architecture: all
Depends:
 ${misc:Depends}
Description: JFC/Swing graph component for Java
 JGraph is an easy-to-use, feature-rich and standards-compliant open source
 graph component available for Java. Application areas include:
 .
  * Process diagrams, workflow and BPM visualization, flowcharts, even
    traffic or water flow.
  * Database and WWW visualization, networks and telecoms displays, mapping
    applications and GIS.
  * UML diagrams, electronic circuits, VLSI, CAD, financial and social
    networks and data mining.
  * Biochemistry, ecological cycles, entity and cause-effect relationships
    and organisational charts.
 .
 This package contains the library.

Package: libjgraph-java-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends}
Description: JFC/Swing graph component for Java (documentation)
 JGraph is an easy-to-use, feature-rich and standards-compliant open source
 graph component available for Java.
 .
  * Process diagrams, workflow and BPM visualization, flowcharts, even
    traffic or water flow.
  * Database and WWW visualization, networks and telecoms displays, mapping
    applications and GIS.
  * UML diagrams, electronic circuits, VLSI, CAD, financial and social
    networks and data mining.
  * Biochemistry, ecological cycles, entity and cause-effect relationships
    and organisational charts.
 .
 This package contains the documentation and the examples.
